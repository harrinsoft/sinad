#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of SINAD (https://gitlab.com/harrinsoft/sinad).
#
# sinad is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the License,or
# any later version.
#
# sinad:
# Is a  ... written in python and QT.
#
# sinad is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sinad; If not, see <http://www.gnu.org/licenses/>.
# _____________________________________________________________________________