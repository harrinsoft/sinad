#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of SINAD (https://gitlab.com/harrinsoft/sinad).
#
# sinad is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the License,or
# any later version.
#
# sinad:
# Is a  ... written in python and QT.
#
# sinad is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sinad; If not, see <http://www.gnu.org/licenses/>.
# _____________________________________________________________________________
import sys
import os.path as path
import random
import subprocess
import traceback
import logging
from pynput.keyboard import Key, Controller, Listener
#with open("styles.css") as f:
#    self.setStyleSheet(f.read())
ch = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
      "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

ch_up = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
         "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

symb = ["-", "*", "!", "_", "(", ")", "/", "¡", "?",
        "¿", "=", ".", ",", ":", "&", "$", "#"]

stack: list = []

def random_password(word_len: int, word_type: int = 2) -> str:
    """generate a random password from the parameters word_len and word_type.

    :parameter:
        word_len:   a number that represents the size of the generated password.
        word_type:  default is 2, if the value is different then it will not
                    include symbols in the password.

    :return:
        a string size (word_len) and symbols if (word_type) is == 2
    """
    password = ""
    charaters = ch + symb + ch_up + num if word_type == 2 else ch + num + ch_up
    for _ in range(word_len):
        password += charaters[random.randint(0, len(charaters) - 1)]
    return password


def check_file_config() -> bool:
    """check if the configuration file exists

    :return:
        True for success, False otherwise.
    """
    logger = logging.getLogger(__name__)
    logger.debug("checking configuration file.")
    _dir = os.getenv("HOME") + "/.config/GuardKey/GK.config"
    return True if path.isfile(_dir) else False


def check_file_db() -> bool:
    """check if the database file exists

    :return:
        True for success, False otherwise.
    """
    logger = logging.getLogger(__name__)
    logger.debug("checking configuration file.")
    _dir = os.getenv("HOME") + "/.config/GuardKey/db.data"
    return True if path.isfile(_dir) else False


def create_file_config() -> None:
    """create an initial configuration file, request a master password.
    """
    logger = logging.getLogger(__name__)
    logger.debug("creating configuration file.")
    _dir = os.getenv("HOME") + "/.config/GuardKey"
    path_file = _dir + "/GK.config"
    try:
        cmd = "mkdir -p {0}".format(_dir)
        subprocess.call(cmd.split())
        logger.debug("file {} created correctly.".format(path_file))
    except Exception:
        logger.error("Exception occurred", exc_info=True)
        traceback.print_exc(file=sys.stdout)


def on_press(key: object) -> bool:
    global stack
    if key == Key.insert:
        if len(stack) > 0:
            Controller().type(stack.pop())
        else: 
            return False
    else:
        if len(stack) == 0:
            return False

def on_release(key: object) -> bool:
    if key == Key.esc:
        return False


def auto_fill(args: list):
    global stack
    stack.append(args[1])
    stack.append(args[0])
    with Listener(
        on_press=on_press,
        on_release=on_release) as listener:
        listener.join()
