#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of sinad (https://gitlab.com/harrinsoft/sinad).
#
# sinad is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the License,or
# any later version.
#
# sinad:
# Is a application to facilitate personnel management in the human 
# resources department written in python and QT5.
#
# sinad is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sinad; If not, see <http://www.gnu.org/licenses/>.
# _____________________________________________________________________________

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import (QLabel, QTextEdit, QComboBox, QListWidget)
from PyQt5.QtWidgets import (QDateEdit, QGraphicsWidget, QHBoxLayout)
from PyQt5.QtWidgets import (QVBoxLayout)

class Dpersonal(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self._setui()
    
    def setui():
        data = {
            'foto': 'img',
            'nombre': 'str',
            'apellido': 'str',
            'cedula': 'int',
            'telefono': 'str',
            'Fecha_Nacimiento': 'datatime',
            'correo': 'str',
            'departamento': 'combo',
            'cargo': 'combo',
            'documentos': 'list'
            }

        for x in data:
        # print ('{0} = {1}'.format(x, data[x]))
            if data(x) == 'str':
                item = QLabel(x)
                item.setObjectName(x)
            elif data(x) == 'img':
                item = QGraphicsWidget()
                item.setObjectName(x)
            elif data(x) == 'combo':
                item = QComboBox()
                item.setObjectName(x)
            
            
            



        

        


