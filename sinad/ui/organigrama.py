# -*- coding: utf-8 -*-
import os
from PyQt5.QtCore import (Qt, pyqtSlot)
from PyQt5.QtWidgets import (QDialog, QListWidget, QPushButton)
from sinad.ui.Detalles_organigrama import OrganigramaDetallesWinUI
from sinad.db.model import get_all_departamentos


class OrganigramaWinUI(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent, Qt.Dialog)
        self.resize(400, 308)
        self.setWindowTitle("Organigrama [SINAD]")

        dir = os.path.dirname(os.path.abspath(__file__))
        with open(dir + "/theme.qss") as qss:
            self.setStyleSheet(qss.read())

        self.setupUi()

    def setupUi(self):
        self.dpto_list = QListWidget(self)
        self.dpto_list.setGeometry(10, 10, 381, 251)
        self.dpto_list.setObjectName("dpto_list")
        self.dpto_list.addItems(get_all_departamentos())

        self.agregar_departamento_pb = QPushButton(self)
        self.agregar_departamento_pb.setGeometry(300, 270, 88, 34)
        self.agregar_departamento_pb.setObjectName("agregar_departamento_pb")
        self.agregar_departamento_pb.setText("Agregar")
        self.agregar_departamento_pb.clicked.connect(self.add_organigrama_detalle_ui)

        self.editar_departamento_pb = QPushButton(self)
        self.editar_departamento_pb.setGeometry(110, 270, 88, 34)
        self.editar_departamento_pb.setObjectName("editar_departamento_pb")
        self.editar_departamento_pb.setText("Editar")
        self.editar_departamento_pb.clicked.connect(self.edit_organigrama_detalle_ui)
        
        self.cerrar_pb = QPushButton(self)
        self.cerrar_pb.setGeometry(10, 270, 88, 34)
        self.cerrar_pb.setObjectName("cerrar_pb")
        self.cerrar_pb.setText("Cerrar")
        self.cerrar_pb.clicked.connect(self.close)

    def add_organigrama_detalle_ui(self):
        win_organigrama_detalles = OrganigramaDetallesWinUI(self)
        win_organigrama_detalles.update.connect(self.updated_list)
        win_organigrama_detalles.show()

    def edit_organigrama_detalle_ui(self):
        cr = self.dpto_list.currentItem()
        id = cr.text().split(" ")
        win_organigrama_detalles = OrganigramaDetallesWinUI(self, id[0])
        win_organigrama_detalles.update.connect(self.updated_list)
        win_organigrama_detalles.show()

    @pyqtSlot()
    def updated_list(self):
        self.dpto_list.clear()
        self.dpto_list.addItems(get_all_departamentos())
