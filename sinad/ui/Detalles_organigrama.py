# -*- coding: utf-8 -*-
import os
from PyQt5.QtCore import (Qt, pyqtSignal)
from PyQt5.QtWidgets import (QApplication , QPushButton)
from PyQt5.QtWidgets import (QDialog, QLineEdit, QLabel, QFrame, QComboBox)
from sinad.db.model import agregar_departamento, editar_departamento
from sinad.db.model import get_departamento, get_all_departamentos


class OrganigramaDetallesWinUI(QDialog):

    update = pyqtSignal()

    def __init__(self, parent=None, aux=None):
        super().__init__(parent, Qt.Dialog)
        self.resize(400, 75)
        self.aux = aux
        self.setWindowTitle("Organigrama Detalles [SINAD]")

        dir = os.path.dirname(os.path.abspath(__file__))
        with open(dir + "/theme.qss") as qss:
            self.setStyleSheet(qss.read())

        self.setupUi()
        self.load_item()

    def setupUi(self):
        self.label = QLabel(self)
        self.label.setGeometry(5, 5, 80, 30)
        self.label.setFrameShape(QFrame.Box)
        self.label.setTextFormat(Qt.AutoText)
        self.label.setObjectName("label")
        self.label.setText("Nombre")

        self.nombre_departamento_le = QLineEdit(self)
        self.nombre_departamento_le.setGeometry(90, 5, 305, 30)
        self.nombre_departamento_le.setObjectName("nombre_departamento_le")
        self.nombre_departamento_le.setText("{0}".format(self.aux))
        
        self.label_8 = QLabel(self)
        self.label_8.setGeometry(5, 40, 80, 30)
        self.label_8.setFrameShape(QFrame.Box)
        self.label_8.setTextFormat(Qt.AutoText)
        self.label_8.setObjectName("label_8")
        self.label_8.setText("Adscrito a:")

        self.dependencia_cb = QComboBox(self)
        self.dependencia_cb.setGeometry(90, 40, 200, 30)
        self.dependencia_cb.setObjectName("dependencia_cb")
        self.dependencia_cb.addItems(get_all_departamentos())

        self.acceptar = QPushButton(self)
        self.acceptar.setGeometry(295, 40, 100, 30)
        self.acceptar.setText("Aceptar")
        self.acceptar.clicked.connect(self.save_chage)
   
    def load_item(self):
        if self.aux:
            data = get_departamento(int(self.aux))
            self.nombre_departamento_le.setText(data[0])
            self.dependencia_cb.setCurrentIndex(data[1])


    def save_chage(self):
        datos = []
        if not self.aux:
            datos.append(self.nombre_departamento_le.text())
            if self.dependencia_cb.currentText() == "":
                datos.append(0)
            else:
                ct = self.dependencia_cb.currentText()
                dependecia_id = ct.split(" ")
                datos.append(int(dependecia_id[0]))

            agregar_departamento(datos)
            self.update.emit()
            self.close()
        else:
            datos.append(self.aux)
            datos.append(self.nombre_departamento_le.text())
            if self.dependencia_cb.currentText() == "":
                datos.append(0)
            else:
                ct = self.dependencia_cb.currentText()
                dependecia_id = ct.split(" ")
                datos.append(int(dependecia_id[0]))

            editar_departamento(datos)
            self.update.emit()
            self.close()

def main() -> None:
    import sys
    app = QApplication(sys.argv)
    main_ui = OrganigramaDetallesWinUI()

    main_ui.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()