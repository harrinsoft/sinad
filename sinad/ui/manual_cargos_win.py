#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of SINAD (https://gitlab.com/harrinsoft/SINAD).
#
# SINAD is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the License,or
# any later version.
#
# SINAD:
# Is a ....  written in python.
#
# SINAD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SINAD; If not, see <http://www.gnu.org/licenses/>.
# _____________________________________________________________________________
import os
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QDialog, QPushButton)
from PyQt5.QtWidgets import (QComboBox, QLabel, QApplication)
from PyQt5.QtWidgets import (QTextEdit, QLineEdit, QFrame)
from sinad.db.model import get_all_cargos, agregar_cargo, editar_cargo 
from sinad.db.model import borrar_cargo, get_cargo


class ManualCargosWinUI(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent, Qt.Dialog)
        self.resize(465, 265)
        self.setWindowTitle("Manual de Cargos [SINAD]")
        self.operacion = None
        dir = os.path.dirname(os.path.abspath(__file__))
        with open(dir + "/theme.qss") as qss:
            self.setStyleSheet(qss.read())

        self.setupUi()

    def setupUi(self):
        self.cargos_cb = QComboBox(self)
        self.cargos_cb.setGeometry(5, 5, 200, 30)
        self.cargos_cb.setObjectName("cargos_cb")
        self.cargos_cb.addItems(get_all_cargos())
        self.cargos_cb.currentTextChanged.connect(self.load_data)

        self.agregar = QPushButton(self)
        self.agregar.setGeometry(210, 5, 80, 30)
        self.agregar.setText("Agregar")
        self.agregar.clicked.connect(self.new_cargo)  

        self.editar = QPushButton(self)
        self.editar.setGeometry(295, 5, 80, 30)
        self.editar.setText("editar")
        self.editar.setEnabled(True)
        self.editar.clicked.connect(self.edit_cargo) 

        self.borrar = QPushButton(self)
        self.borrar.setGeometry(380, 5, 80, 30)
        self.borrar.setText("borrar")
        self.borrar.setEnabled(False)
        self.borrar.clicked.connect(self.del_cargo) 

        self.label = QLabel(self)
        self.label.setGeometry(5, 40, 80, 30)
        self.label.setFrameShape(QFrame.Box)
        self.label.setTextFormat(Qt.AutoText)
        self.label.setText("Nombre")

        self.nombre_cargo_le = QLineEdit(self)
        self.nombre_cargo_le.setGeometry(90, 40, 285, 30)
        self.nombre_cargo_le.setText("")
        self.nombre_cargo_le.setEnabled(False)

        self.guardar = QPushButton(self)
        self.guardar.setGeometry(380, 40, 70, 30)
        self.guardar.setText("guardar")
        self.guardar.setEnabled(False)
        self.guardar.clicked.connect(self.updated)

        self.label = QLabel(self)
        self.label.setGeometry(5, 75, 455, 30)
        self.label.setFrameShape(QFrame.Box)
        self.label.setTextFormat(Qt.AutoText)
        self.label.setText("Descripcion")

        self.descripcion_cargo_te = QTextEdit(self)
        self.descripcion_cargo_te.setGeometry(5, 110, 455, 150)
        self.descripcion_cargo_te.setText("")
        self.descripcion_cargo_te.setEnabled(False)

        self.load_data()

    def new_cargo(self):
        self.operacion = "add"
        self.editar.setEnabled(False)
        self.agregar.setEnabled(False)
        self.guardar.setEnabled(True)
        self.cargos_cb.setEnabled(False)
        self.nombre_cargo_le.setEnabled(True)
        self.nombre_cargo_le.setText("")
        self.descripcion_cargo_te.setEnabled(True)
        self.descripcion_cargo_te.setText("")

    def edit_cargo(self):
        self.operacion = "edit"
        self.editar.setEnabled(False)
        self.agregar.setEnabled(False)
        self.guardar.setEnabled(True)
        self.nombre_cargo_le.setEnabled(True)
        self.descripcion_cargo_te.setEnabled(True)
        self.cargos_cb.setEnabled(False)

    def del_cargo(self):
        borrar_cargo(get_cargo(self.cargos_cb.currentText())[0])
        self.cargos_cb.clear()
        self.cargos_cb.addItems(get_all_cargos())

    def updated(self):
        data = []
        if self.operacion == "add":
            data.append(self.nombre_cargo_le.text())
            data.append(self.descripcion_cargo_te.toPlainText())
            data.append(None)
            agregar_cargo(data)
            self.operacion = None
            self.agregar.setEnabled(True)
            self.guardar.setEnabled(False)
            self.editar.setEnabled(True)
            self.cargos_cb.setEnabled(True)
            self.nombre_cargo_le.setEnabled(False)
            self.descripcion_cargo_te.setEnabled(False)
        elif self.operacion == "edit":
            data = []
            cargo_to_edit = get_cargo(self.cargos_cb.currentText())
            data.append(cargo_to_edit[0])
            data.append(self.nombre_cargo_le.text())
            data.append(self.descripcion_cargo_te.toPlainText())
            data.append(None)
            editar_cargo(data)
            self.operacion = None
            self.agregar.setEnabled(True)
            self.cargos_cb.setEnabled(True)
            self.editar.setEnabled(True)
            self.guardar.setEnabled(False)
            self.nombre_cargo_le.setEnabled(False)
            self.descripcion_cargo_te.setEnabled(False)

        self.cargos_cb.clear()
        self.cargos_cb.addItems(get_all_cargos())

    def load_data(self):
        if self.cargos_cb.currentText():
            data = get_cargo(self.cargos_cb.currentText())
            self.nombre_cargo_le.setText(data[1])
            self.descripcion_cargo_te.setText(data[2])


def main() -> None:
    import sys
    app = QApplication(sys.argv)
    main_ui = ManualCargosWinUI()

    main_ui.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()