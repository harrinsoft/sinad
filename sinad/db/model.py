#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of SINAD (https://gitlab.com/harrinsoft/SINAD).
#
# SINAD is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the License,or
# any later version.
#
# SINAD:
# Is a ....  written in python.
#
# SINAD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SINAD; If not, see <http://www.gnu.org/licenses/>.
# _____________________________________________________________________________
import os
import os.path as path
import logging
import subprocess
from datetime import datetime
from pony.orm import *


db = Database()
_dir = os.getenv("HOME") + "/.config/sinad"

if not path.isdir(_dir):
    cmd = "mkdir -p {0}".format(_dir)
    subprocess.call(cmd.split())


class Departamento(db.Entity):
    id = PrimaryKey(int, auto=True)
    nombre = Optional(str)
    adscripcion = Optional(int)


class Personal(db.Entity):
    id = PrimaryKey(int, auto=True)
    foto = Optional(str)
    nombre = Optional(str)
    apellido = Optional(str)
    cedula = Optional(int)
    telefono = Optional(str)
    Fecha_nacimiento = Optional(datetime)
    correo = Optional(str)
    departamento = Required(int)
    cargo = Required(int)
    documentos = Required(int)


class Cargo(db.Entity):
    id = PrimaryKey(int, auto=True)
    nombre = Optional(str)
    descripcion = Optional(str)
    personal = Optional(int)


class Documento(db.Entity):
    id = PrimaryKey(int, auto=True)
    nombre = Optional(str)
    descripcion = Optional(str)
    url = Optional(str)
    personal = Required(int)


db.bind(provider='sqlite',
        filename="{0}/db.data".format(_dir),
        create_db=True)

db.generate_mapping(create_tables=True)

@db_session
def get_all_departamentos() -> list:
    departamentos = []
    
    dptos = Departamento.select(lambda d: d.nombre is not None)
    for d in dptos:
        departamentos.append("{0} - {1}".format(d.id, d.nombre))
        
    return departamentos

@db_session
def get_departamento(data: int) -> list:
    d = []
    dpto = Departamento.get(id=data)
    d.append(dpto.nombre)
    d.append(dpto.adscripcion)

    return d

@db_session
def agregar_departamento(data: list) -> None:
    departamento_nuevo = Departamento(
        nombre = data[0],
        adscripcion = data[1]
    )

@db_session
def editar_departamento(data: list) -> None:
    departamento = Departamento.get(id=data[0])
    departamento.nombre = data[1]
    departamento.adscripcion = data[2]

@db_session
def borrar_departamento(data: int) -> None:
    Departamento.get(id=data).delete()


@db_session
def get_all_cargos() -> list:
    c = []
    cargos = Cargo.select(lambda x: x.nombre is not None)

    for cargo in cargos:
        c.append(cargo.nombre)

    return c

@db_session
def get_cargo(data: str) -> list:
    c = []
    cargo = Cargo.get(nombre=data)
    c.append(cargo.id)
    c.append(cargo.nombre)
    c.append(cargo.descripcion)
    c.append(cargo.personal)

    return c

@db_session
def agregar_cargo(data: list) -> None:
    cargo_nuevo = Cargo(
        nombre=data[0],
        descripcion=data[1],
        personal=data[2]
    )

@db_session
def editar_cargo(data: list) -> None:
    cargo = Cargo.get(id=data[0])
    cargo.nombre = data[1]
    cargo.descripcion = data[2]
    cargo.personal = data[3]

@db_session
def borrar_cargo(data: int) -> None:
    Cargo.get(id=data).delete()