#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of SINAD (https://gitlab.com/harrinsoft/sinad).
#
# sinad is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the License,or
# any later version.
#
# sinad:
# Is a  ... written in python and QT.
#
# sinad is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sinad; If not, see <http://www.gnu.org/licenses/>.
# _____________________________________________________________________________
# -----------------------------------------------------------------------------
#   META DATA
# -----------------------------------------------------------------------------


class Version:

    major = 0
    minor = 0
    micro = 1
    

    def get_version(self):
        return '{0}.{1}.{2}'.format(self.major, self.minor, self.micro)

    def __str__(self):
        return "sinad {0}.{1}.{2}".format(self.major, self.minor, self.micro)

    def __repr__(self):
        return "{0}.{1}.{2}".format(self.major, self.minor, self.micro)


class Autor:

    alias = 'harrinsoft'
    name = 'Harrinson Gavidia'
    email = 'harrinsoft@gmail.com'

    def __str__(self):
        return "{0} -[{1}]-".format(self.alias, self.email)


class AppInfo:
    name = 'sinad'
    description = 'Is a  ... written in python and QT'
    url = 'https://gitlab.com/harrinsoft/sinad.git'
    license = 'GPLv3'


version = Version()
autor = Autor()
info = AppInfo()
