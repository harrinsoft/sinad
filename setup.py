
from setuptools import setup, find_packages
import sinad

VERSION = sinad.version.get_version()

f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name=sinad.info.name,
    version=VERSION,
    description=sinad.info.description,
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author="{0} ({1})".format(sinad.autor.name, sinad.autor.alias),
    author_email=sinad.autor.email,
    url=sinad.info.url,
    license=sinad.info.license,
    packages=find_packages(),
    include_package_data = True,
    package_data = {
        '': ['*.qss'],
        '': ['ui/*.qss'],
        'ui': ['*.qss'],
    },
    entry_points="""
        [console_scripts]
        sinad = sinad.main:main
    """
)
